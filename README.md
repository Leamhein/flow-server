https://habr.com/company/ruvds/blog/340750/

API:

// Register User:

POST /api/v1/signup
Headers: {
  Content-Type: application/x-www-form-urlencoded,
}
Body: {
  username: String,
  password: String,
  email: String,
}

Answer: {
    "success": true,
    "message": "Account created successfully"
}

// Auth User:

POST /api/v1/auth
Headers: {
  Content-Type: application/x-www-form-urlencoded,
}
Body: {
  username: String,
  password: String,
}

Answer: {
    "success": true,
    "message": "Token granted",
    "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7Il9pZCI6IjVjMmRlOTlkYzFjN2ZlMTczMDczYmY0ZCIsInVzZXJuYW1lIjoiTGVhbWhlaW4iLCJwYXNzd29yZCI6IiQyYiQxMiRLSTdITDNINUcxZld6aXMxMHFwV091RldyWDlLTHdVemdHaVVQNjg2azJsVmI3VFR2NktxbSIsIl9fdiI6MH0sImlhdCI6MTU0NjUxODYwMX0.tM3TgWI5eHf0XyDE4jSFEiQbNPmYPlxttTYsX5ISZIg",
    "user_id": "5c2de99dc1c7fe173073bf4d"
}

// send user password reset email

POST /api/v1/reset
Headers: {
  Content-Type: application/x-www-form-urlencoded,
}
Body: {
  email: String
}

Answer: {
    "success": true,
    "message": "An e-mail has been sent to {email} with further instructions."
}

// change user password

PUT /api/v1/reset?token=[string]
Headers: {
  Content-Type: application/x-www-form-urlencoded,
}
Body: {
  password: String
}

Answer: {
    "success": true,
    "message": "An e-mail has been sent to [email]. Password successfully changed."
}

// Add Bookkeeping:

POST /api/v1/bookkeeping
Headers: {
  Content-Type: application/x-www-form-urlencoded,
  Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7Il9pZCI6IjVjMmRlOTlkYzFjN2ZlMTczMDczYmY0ZCIsInVzZXJuYW1lIjoiTGVhbWhlaW4iLCJwYXNzd29yZCI6IiQyYiQxMiRLSTdITDNINUcxZld6aXMxMHFwV091RldyWDlLTHdVemdHaVVQNjg2azJsVmI3VFR2NktxbSIsIl9fdiI6MH0sImlhdCI6MTU0NjUxNDU2Mn0.T7MTVV6cwgo08Nb8rZwqIx-wr889eSfIKKvyGhJBOuE
}
Body: {
  user_id: String,
  title: String,
  description: String,
  currency: String,
  icon: String,
}

Answer: {
    "success": true,
    "message": "Bookkeeping successfully added",
    "bookkeeping_id": "5c2e149c7604581c5c7bd881"
}

// Get Bookkeeping by Id:

GET `/api/v1/bookkeeping/single?user_id=${number}&bookkeeping_id=${number}`
Headers: {
  Content-Type: application/x-www-form-urlencoded,
  Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7Il9pZCI6IjVjMmRlOTlkYzFjN2ZlMTczMDczYmY0ZCIsInVzZXJuYW1lIjoiTGVhbWhlaW4iLCJwYXNzd29yZCI6IiQyYiQxMiRLSTdITDNINUcxZld6aXMxMHFwV091RldyWDlLTHdVemdHaVVQNjg2azJsVmI3VFR2NktxbSIsIl9fdiI6MH0sImlhdCI6MTU0NjUxNDU2Mn0.T7MTVV6cwgo08Nb8rZwqIx-wr889eSfIKKvyGhJBOuE
}

Answer: {
    "wallets": [],
    "_id": "5c2e010af5c15c4708dc4cae",
    "user_id": "5c2de99dc1c7fe173073bf4d",
    "title": "Leamhein wallet",
    "description": "Leamhein account",
    "currency": "EUR",
    "icon": "myIconTest",
    "__v": 0
}

// Get All Bookkeeping

GET `/api/v1/bookkeeping?user_id=${number}`
Headers: {
  Content-Type: application/x-www-form-urlencoded,
  Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7Il9pZCI6IjVjMmRlOTlkYzFjN2ZlMTczMDczYmY0ZCIsInVzZXJuYW1lIjoiTGVhbWhlaW4iLCJwYXNzd29yZCI6IiQyYiQxMiRLSTdITDNINUcxZld6aXMxMHFwV091RldyWDlLTHdVemdHaVVQNjg2azJsVmI3VFR2NktxbSIsIl9fdiI6MH0sImlhdCI6MTU0NjUxNDU2Mn0.T7MTVV6cwgo08Nb8rZwqIx-wr889eSfIKKvyGhJBOuE
}

Answer: [
    {
        "wallets": [],
        "_id": "5c2e00b5b3cd193cecee92b8",
        "user_id": "5c2de99dc1c7fe173073bf4d",
        "title": "Leamhein Bookkeeping 3",
        "description": "Leamhein account",
        "currency": "BYR",
        "icon": "myIconTest",
        "__v": 0
    },
    {
        "wallets": [],
        "_id": "5c2e010af5c15c4708dc4cae",
        "user_id": "5c2de99dc1c7fe173073bf4d",
        "title": "Leamhein wallet",
        "description": "Leamhein account",
        "currency": "EUR",
        "icon": "myIconTest",
        "__v": 0
    }
]

// Edit Bookkeeping

PUT `/api/v1/bookkeeping/single?user_id=${number}&bookkeeping_id=${number}`
Headers: {
  Content-Type: application/x-www-form-urlencoded,
  Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7Il9pZCI6IjVjMmRlOTlkYzFjN2ZlMTczMDczYmY0ZCIsInVzZXJuYW1lIjoiTGVhbWhlaW4iLCJwYXNzd29yZCI6IiQyYiQxMiRLSTdITDNINUcxZld6aXMxMHFwV091RldyWDlLTHdVemdHaVVQNjg2azJsVmI3VFR2NktxbSIsIl9fdiI6MH0sImlhdCI6MTU0NjUxNDU2Mn0.T7MTVV6cwgo08Nb8rZwqIx-wr889eSfIKKvyGhJBOuE
}
Body: {
  user_id?: String | String[],
  title?: String,
  description?: String,
  currency?: String,
  icon?: String,
}

Answer: {
    "wallets": [],
    "_id": "5c2e010af5c15c4708dc4cae",
    "user_id": "5c2de99dc1c7fe173073bf4d",
    "title": "Leamhein wallet",
    "description": "Leamhein account",
    "currency": "EUR",
    "icon": "myIconTest",
    "__v": 0
}

// Delete bookkeeping

DELETE `/api/v1/bookkeeping?user_id=${number}&bookkeeping_id=${number}`
Headers: Headers: {
  Content-Type: application/x-www-form-urlencoded,
  Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7Il9pZCI6IjVjMmRlOTlkYzFjN2ZlMTczMDczYmY0ZCIsInVzZXJuYW1lIjoiTGVhbWhlaW4iLCJwYXNzd29yZCI6IiQyYiQxMiRLSTdITDNINUcxZld6aXMxMHFwV091RldyWDlLTHdVemdHaVVQNjg2azJsVmI3VFR2NktxbSIsIl9fdiI6MH0sImlhdCI6MTU0NjUxNDU2Mn0.T7MTVV6cwgo08Nb8rZwqIx-wr889eSfIKKvyGhJBOuE
}

Answer: {
    "success": true,
    "message": "Removed successfully"
}

// Add wallet

POST /api/v1/wallet
Headers: {
  Content-Type: application/x-www-form-urlencoded,
  Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7Il9pZCI6IjVjMmRlOTlkYzFjN2ZlMTczMDczYmY0ZCIsInVzZXJuYW1lIjoiTGVhbWhlaW4iLCJwYXNzd29yZCI6IiQyYiQxMiRLSTdITDNINUcxZld6aXMxMHFwV091RldyWDlLTHdVemdHaVVQNjg2azJsVmI3VFR2NktxbSIsIl9fdiI6MH0sImlhdCI6MTU0NjUxNDU2Mn0.T7MTVV6cwgo08Nb8rZwqIx-wr889eSfIKKvyGhJBOuE
}
Body: {
  bookkeeping_id: bookkeeping._id,
  title: req.body.title,
  startBalance: req.body.startBalance,
  icon: req.body.icon,
}

Answer: {
    "success": true,
    "message": "Wallet successfully registered",
    "wallet_id": "5c2e1a3038c23a4714e91329"
}

// Get wallet by Id

GET `/api/v1/wallet/single?user_id=${number}&wallet_id=${number}`
Headers: Headers: {
  Content-Type: application/x-www-form-urlencoded,
  Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7Il9pZCI6IjVjMmRlOTlkYzFjN2ZlMTczMDczYmY0ZCIsInVzZXJuYW1lIjoiTGVhbWhlaW4iLCJwYXNzd29yZCI6IiQyYiQxMiRLSTdITDNINUcxZld6aXMxMHFwV091RldyWDlLTHdVemdHaVVQNjg2azJsVmI3VFR2NktxbSIsIl9fdiI6MH0sImlhdCI6MTU0NjUxNDU2Mn0.T7MTVV6cwgo08Nb8rZwqIx-wr889eSfIKKvyGhJBOuE
}

Answer: {
    "transactions": [],
    "transfers": [],
    "_id": "5c2e01acf54c842e54fb4e90",
    "user_id": "5c2de99dc1c7fe173073bf4d",
    "bookkeeping_id": "5c2e010af5c15c4708dc4cae",
    "title": "Leamhein wallet",
    "startBalance": 2312,
    "icon": "myIconTest",
    "__v": 0
}

// Get wallets by bookkeeping

GET `/api/v1/wallet?bookkeeping_id=${number}`
Headers: Headers: {
  Content-Type: application/x-www-form-urlencoded,
  Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7Il9pZCI6IjVjMmRlOTlkYzFjN2ZlMTczMDczYmY0ZCIsInVzZXJuYW1lIjoiTGVhbWhlaW4iLCJwYXNzd29yZCI6IiQyYiQxMiRLSTdITDNINUcxZld6aXMxMHFwV091RldyWDlLTHdVemdHaVVQNjg2azJsVmI3VFR2NktxbSIsIl9fdiI6MH0sImlhdCI6MTU0NjUxNDU2Mn0.T7MTVV6cwgo08Nb8rZwqIx-wr889eSfIKKvyGhJBOuE
}

Answer: [{
    "transactions": [],
    "transfers": [],
    "_id": "5c2e01acf54c842e54fb4e90",
    "user_id": "5c2de99dc1c7fe173073bf4d",
    "bookkeeping_id": "5c2e010af5c15c4708dc4cae",
    "title": "Leamhein wallet",
    "startBalance": 2312,
    "icon": "myIconTest",
    "__v": 0
}]

// Get All wallets

GET `/api/v1/wallet?user_id=${number}`
Headers: {
  Content-Type: application/x-www-form-urlencoded,
  Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7Il9pZCI6IjVjMmRlOTlkYzFjN2ZlMTczMDczYmY0ZCIsInVzZXJuYW1lIjoiTGVhbWhlaW4iLCJwYXNzd29yZCI6IiQyYiQxMiRLSTdITDNINUcxZld6aXMxMHFwV091RldyWDlLTHdVemdHaVVQNjg2azJsVmI3VFR2NktxbSIsIl9fdiI6MH0sImlhdCI6MTU0NjUxNDU2Mn0.T7MTVV6cwgo08Nb8rZwqIx-wr889eSfIKKvyGhJBOuE
}

Answer: [{
    "transactions": [],
    "transfers": [],
    "_id": "5c2e01acf54c842e54fb4e90",
    "user_id": "5c2de99dc1c7fe173073bf4d",
    "bookkeeping_id": "5c2e010af5c15c4708dc4cae",
    "title": "Leamhein wallet",
    "startBalance": 2312,
    "icon": "myIconTest",
    "__v": 0
}]

// Edit wallet
PUT /api/v1/wallet/single
Headers: {
  Content-Type: application/x-www-form-urlencoded,
  Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7Il9pZCI6IjVjMmRlOTlkYzFjN2ZlMTczMDczYmY0ZCIsInVzZXJuYW1lIjoiTGVhbWhlaW4iLCJwYXNzd29yZCI6IiQyYiQxMiRLSTdITDNINUcxZld6aXMxMHFwV091RldyWDlLTHdVemdHaVVQNjg2azJsVmI3VFR2NktxbSIsIl9fdiI6MH0sImlhdCI6MTU0NjUxNDU2Mn0.T7MTVV6cwgo08Nb8rZwqIx-wr889eSfIKKvyGhJBOuE
}
Body: {
  user_id: Number,
  wallet_id: Number,
  bookkeeping_id?: bookkeeping._id,
  title?: req.body.title,
  startBalance?: req.body.startBalance,
  icon?: req.body.icon,
}

Answer: {
    "transactions": [],
    "transfers": [],
    "_id": "5c2e1a3038c23a4714e91329",
    "user_id": "5c2de99dc1c7fe173073bf4d",
    "bookkeeping_id": "5c2e00b5b3cd193cecee92b8",
    "title": "Leamhein wallet",
    "startBalance": 2312,
    "icon": "myIconTest",
    "__v": 0
}

// Delete wallet
DELETE `/api/v1/wallet?wallet_id=${number}`
Headers: {
  Content-Type: application/x-www-form-urlencoded,
  Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7Il9pZCI6IjVjMmRlOTlkYzFjN2ZlMTczMDczYmY0ZCIsInVzZXJuYW1lIjoiTGVhbWhlaW4iLCJwYXNzd29yZCI6IiQyYiQxMiRLSTdITDNINUcxZld6aXMxMHFwV091RldyWDlLTHdVemdHaVVQNjg2azJsVmI3VFR2NktxbSIsIl9fdiI6MH0sImlhdCI6MTU0NjUxNDU2Mn0.T7MTVV6cwgo08Nb8rZwqIx-wr889eSfIKKvyGhJBOuE
}

Answer: {
    "success": true,
    "message": "Removed successfully"
}
