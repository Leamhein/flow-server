const parsedUrl = new URL(window.location.href);
const token = parsedUrl.searchParams.get('token');
const sendBtn = document.getElementById('send-btn');
const pasInput = document.getElementById('new-pas');
const confInput = document.getElementById('conf-pas');
const alert = document.getElementById('alert');
sendBtn.onclick = () => {
  if (pasInput.value !== confInput.value) {
    alert.classList.add('alert-fail');
    alert.innerHTML = null;
    const text = document.createElement('p');
    text.innerHTML = 'Passwords must match';
    alert.appendChild(text);
    console.error('Passwords must match');
    return;
  }
  if (pasInput.value.length < 5) {
    alert.classList.add('alert-fail');
    alert.innerHTML = null;
    const text = document.createElement('p');
    text.innerHTML = 'Password must be at least 5 characters long';
    alert.appendChild(text);
    console.error('Password must be at least 5 characters long');
    return;
  }

  const myHeaders = new Headers();
  myHeaders.append('Content-Type', 'application/json');

  const myInit = {
    method: 'PUT',
    headers: myHeaders,
    body: JSON.stringify({
      password: pasInput.value
    }),
    mode: 'cors'
  };
  const myRequest = new Request(window.location.href, myInit);
  fetch(myRequest).then((response) => {
    alert.classList.remove('alert-fail');
    if (response.status === 200) {
      alert.classList.add('alert-success');
      alert.innerHTML = null;
      const text = document.createElement('p');
      text.innerHTML = 'Password successfully changed.';
      alert.appendChild(text);
    } else {
      alert.classList.add('alert-fail');
      alert.innerHTML = null;
      const text = document.createElement('p');
      text.innerHTML = `Response status is ${response.status}!`;
      alert.appendChild(text);
    }
    return response.json();
  });
};
