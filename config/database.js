module.exports = (mongoose, config) => {
  const database = mongoose.connection;
  mongoose.Promise = Promise;
  mongoose.connect(config.database, {
    useCreateIndex: true,
    useNewUrlParser: true,
    promiseLibrary: global.Promise,
  });
  database.on('error', error => console.log(`Connection to Flow database failed: ${error}`));
  database.on('connected', () => console.log('Connected to Flow database'));
  database.on('disconnected', () => console.log('Disconnected from Flow database'));
  process.on('SIGINT', () => {
    database.close(()=>{
      console.log('Flow terminated, connection closed');
      process.exit(0);
    })
  })
}