require('module-alias/register');
const http = require('http');
const FlowAPI = require('@FlowAPI');
const FlowServer = http.Server(FlowAPI);
const PORT = process.env.PORT || 3001;
const LOCAL = '0.0.0.0';
FlowServer.listen(PORT, LOCAL, () => console.log(`FlowAPI running on ${PORT}`));