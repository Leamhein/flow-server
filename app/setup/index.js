const mongoose = require('mongoose');
const UserModel = require('@FlowModels/user');
const BookkeepingModel = require('@FlowModels/bookkeeping');
const WalletModel = require('@FlowModels/wallet');
const models = {
  User: mongoose.model('User'),
  Bookkeeping: mongoose.model('Bookkeeping'),
  Wallet: mongoose.model('Wallet'),
}
module.exports = models;
