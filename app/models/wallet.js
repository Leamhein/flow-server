const mongoose = require('mongoose');

const Schema = mongoose.Schema({
  user_id: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  },
  bookkeeping_id: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Bookkeeping'
  },
  title: {
    type: String,
    required: true,
  },
  startBalance: {
    type: Number,
    required: true,
  },
  icon: {
    type: String,
    required: true,
  },
  transactions: [{}],
  transfers: [{}],
});

mongoose.model('Wallet', Schema);