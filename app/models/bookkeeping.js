const mongoose = require('mongoose');

const Schema = mongoose.Schema({
  user_id: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  },
  title: {
    type: String,
    required: true,
  },
  description: {
    type: String,
    required: true,
  },
  currency: {
    type: String,
    required: true,
  },
  icon: {
    type: String,
    required: true,
  },
  wallets: [{}],
});

mongoose.model('Bookkeeping', Schema);