const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const nodemailer = require('nodemailer');
const api = {};

//! TODO delete admin user in production
api.setup = (User) => (req, res) => {
  const admin = new User({
    username: 'admin',
    password: 'admin',
  });
admin.save(error => {
    if (error) throw error;
    console.log('Admin account was successfully set up');
    res.json({ success: true });
  })
};
// !

api.signup = (User) => (req, res) => {
  if (!req.body.username || !req.body.password) {
    res.json({ success: false, message: 'Please, pass a username, password and email.' });
  } else {
    const newUser = new User({
      username: req.body.username,
      password: req.body.password,
      email: req.body.email,
    });
    newUser.save((error) => {
      if (error) {
        return res.status(400).json({ success: false, message:  error.errors });
      }
      return res.json({ success: true, message: 'Account created successfully' });
    });
  }
};

api.sendResetMail = (User) => (req, res) => {
  User.findOne ({email: req.body.email}, (error, user) => {
    if (error) {
      throw error;
    }
    if (!user) {
      res.status(401).send({success: false, message: 'Password reset failed. Email not found.'});
    } else {
      bcrypt.genSalt(12, (error, salt) => {
        if (error) {
          res.status(400).send({success: false, message: error});
        }
        bcrypt.hash(user.email, salt, (error, hash) => {
          if (error) {
            res.status(400).send({success: false, message: error});
          }
          user.resetPasswordToken = hash;
          user.resetPasswordExpires = Date.now() + 3600000;
          user.save((error) => {
            if (error) {
              return res.status(400).json({ success: false, message:  error });
            }
            const smtpTransporter = nodemailer.createTransport({
              host: 'smtp.ethereal.email',
              port: 587,
              auth: {
                  user: 'abgtm32rx2g7w5z7@ethereal.email',
                  pass: 'EfXsCvVxBqGaXmBV3h'
              }
          });
            const mailOptions = {
              to: user.email,
              from: 'abgtm32rx2g7w5z7@ethereal.email',
              subject: 'Flow Password Reset',
              text: `You are receiving this because you (or someone else) have requested the reset of the password for your account.
                Please click on the following link, or paste this into your browser to complete the process:
                http://${req.headers.host}/api/v1/reset?token=${user.resetPasswordToken}
                If you did not request this, please ignore this email and your password will remain unchanged.`
            };
            smtpTransporter.sendMail(mailOptions, function(err) {
              res.json({ success: true, message: `An e-mail has been sent to ${user.email} with further instructions.` });
            });
          });
        });
      });
    }
  });
};

api.showResetPage = (User) => (req, res) => {
  User.findOne({ resetPasswordToken: req.query.token, resetPasswordExpires: { $gt: Date.now() } }, function(err, user) {
    if (!user) {
      //res.json({message: 'Password reset token is invalid or has expired.'});
      return res.redirect('/expired');
    }
    res.render('reset/reset');
  });
};

api.resetPassword = (User) => (req, res) => {
  User.findOne({ resetPasswordToken: req.query.token, resetPasswordExpires: { $gt: Date.now() } }, (err, user) => {
    if (!user) {
      return res.redirect('/expired');
    }
    if (!req.body.password) {
      res.json({ success: false, message: 'Please, pass a password.' });
    }

    user.password = req.body.password;
    user.resetPasswordToken = null;
    user.resetPasswordExpires = null;

    user.save((error) => {
      if (error) {
        //! 
        console.log(req.body)
        return //res.send({ success: false, message:  error });
      }
      const smtpTransporter = nodemailer.createTransport({
        host: 'smtp.ethereal.email',
        port: 587,
        auth: {
            user: 'abgtm32rx2g7w5z7@ethereal.email',
            pass: 'EfXsCvVxBqGaXmBV3h'
        }
    });
      const mailOptions = {
        to: user.email,
        from: 'abgtm32rx2g7w5z7@ethereal.email',
        subject: 'Flow Password Reset',
        text: `This is a confirmation that the password for your account ${user.email} has just been changed.`
      };
      smtpTransporter.sendMail(mailOptions, function(err) {
        res.json({ success: true, message: `An e-mail has been sent to ${user.email}. Password successfully changed.` });
      });
    });
  })
}

module.exports = api;