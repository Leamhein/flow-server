const mongoose = require('mongoose');

const api = {};

api.store = (User, Bookkeeping, Token) => (req, res) => {
  if (Token) {
    const bookkeeping = new Bookkeeping({
      user_id: req.body.user_id,
      title: req.body.title,
      description: req.body.description,
      currency: req.body.currency,
      icon: req.body.icon,
    });
    bookkeeping.save(error => {
      if (error) {
        return res.status(400).json(error);
      }
      res.status(200).json({
        success: true,
        message: 'Bookkeeping successfully added',
        bookkeeping_id: bookkeeping._id,
      });
    })
  } else {
    return res.status(403).json({
      success: false,
      message: 'Unauthorized',
    });
  }
};

api.getById = (User, Bookkeeping, Token) => (req, res) => {
  if (Token) {
    User.findOne({ _id: req.query.user_id }, (error, user) => {
      if (error) {
        return res.status(400).json(error);
      }

      if (user) {
        Bookkeeping.findOne({ _id: req.query.bookkeeping_id }, (error, bookkeeping) => {
          if (error) {
            return res.status(400).json(error);
          }
          res.status(200).json(bookkeeping);
        })
      } else {
        res.status(400).json({ success: false, message: "Invalid Bookkeeping," })
      }
    })

  } else {
    return res.status(401).send({ success: false, message: 'Unauthorized' });
  }
};

api.getAll = (User, Bookkeeping, Token) => (req, res) => {
  if (Token) {
    Bookkeeping.find({
      user_id: req.query.user_id,
    }, (error, bookkeeping) => {
      if (error) {
        return res.status(400).json(error);
      }
      res.status(200).json(bookkeeping);
      return true;
    })
  } else {
    return res.status(403).json({
      success: false,
      message: 'Unauthorized',
    })
  }
};

api.edit = (User, Bookkeeping, Token) => (req, res) => {
  if (Token) {
    User.findOne({ _id: req.query.user_id }, (error, user) => {
      if (error) {
        return res.status(400).json(error);
      }
      if (user) {
        Bookkeeping.findOneAndUpdate({ _id: req.query.bookkeeping_id }, req.body, (error, bookkeeping) => {
          if (error) {
            return res.status(400).json(error);
          }
          res.status(200).json(bookkeeping);
        })
      } else {
        res.status(400).json({ success: false, message: "Invalid Bookkeeping" })
      }
    })

  } else return res.status(401).send({ success: false, message: 'Unauthorized' });
};
// ! TODO remove dependent wallets
api.remove = (User, Bookkeeping, Token) => (req, res) => {
  if (Token) {
    User.findOne({ _id: req.query.user_id }, (error, user) => {
      if (error) {
        return res.status(400).json(error);
      }
      if (user) {
        Bookkeeping.remove({ _id: req.query.bookkeeping_id }, (error, removed) => {
          if (error) {
            return res.status(400).json(error);
          }
          res.status(200).json({ success: true, message: 'Removed successfully' });
        })
      } else {
        res.status(400).json({ success: false, message: "Invalid Bookkeeping" })
      }
    })

  } else {
    return res.status(401).send({ success: false, message: 'Unauthorized' });
  }
}

module.exports = api;