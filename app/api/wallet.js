const mongoose = require('mongoose');

const api = {};

api.store = (User, Wallet, Bookkeeping, Token) => (req, res) => {
  if (Token) {
    Bookkeeping.findOne({
      _id: req.body.bookkeeping_id,
    }, (error, bookkeeping) => {
      if (error) {
        res.status(400).json(error);
      }
      if (bookkeeping) {
        const wallet = new Wallet({
          user_id: bookkeeping.user_id,
          bookkeeping_id: bookkeeping._id,
          title: req.body.title,
          startBalance: req.body.startBalance,
          icon: req.body.icon,
        });
        wallet.save(error => {
          if (error) {
            return res.status(400).json(error);
          }
          res.status(200).json({
            success: true,
            message: 'Wallet successfully registered',
            wallet_id: wallet._id
          })
        })
      } else {
        res.status(400).json({
          success: false,
          message: 'Invalid client',
        })
      }
    })
  } else {
    return res.status(403).json({
      success: false,
      message: 'Unauthorized',
    });
  }
};

api.getById = (User, Wallet, Bookkeeping, Token) => (req, res) => {
  if (Token) {
    User.findOne({_id: req.query.user_id}, (error, user) => {
      if (error) {
        res.status(400).json(error);
      }
      if (user) {
        Wallet.findOne({_id: req.query.wallet_id}, (error, wallet) => {
          if (error) {
           return res.status(400).json(error);
          }
          res.status(200).json(wallet);
        })
      } else {
        res.status(400).json({ success: false, message: "Invalid budget" });
      }
    })
  } else {
    return res.status(401).send({ success: false, message: 'Unauthorized' });
  }
};

api.getAll = (User, Wallet, Token) => (req, res) => {
  if (Token) {
    if (!req.query.user_id && !req.query.bookkeeping_id) {
      return res.status(400).json({ success: false, message: 'Bad request' });
    }
    Wallet.find({ user_id: req.query.user_id }, (error, wallet) => {
      if (error) {
        return res.status(400).json(error);
      }
      res.status(200).json(wallet);
      return true;
    })
  } else {
    return res.status(403).json({ success: false, message: 'Unauthorized' });
  }
};

api.getAllFromBookkeeping = (User, Wallet, Token) => (req, res) => {
  if (Token) {
    if (!req.query.user_id && !req.query.bookkeeping_id) {
      return res.status(400).json({ success: false, message: 'Bad request' });
    }
    Wallet.find({ bookkeeping_id: req.query.bookkeeping_id }, (error, wallet) => {
      if (error) {
        return res.status(400).json(error);
      }
      res.status(200).json(wallet);
      return true;
    })
  } else {
    return res.status(403).json({ success: false, message: 'Unauthorized' });
  }
};

api.edit = (User, Wallet, Bookkeeping, Token) => (req, res) => {
  if (Token) {
    User.findOne({_id: req.body.user_id}, (error, user) => {
      if (error) {
        return res.status(400).json(error);
      }
      if (user) {
        Wallet.findOneAndUpdate({_id: req.body.wallet_id}, req.body, (error, wallet) => {
          if (error) {
            return res.status(400).json(error);
          }
          res.status(200).json(wallet);
        })
      } else {
        res.status(400).json({ success: false, message: "Invalid wallet" })
      }
    })
  } else {
    return res.status(401).send({ success: false, message: 'Unauthorized' });
  }
};

api.remove = (User, Wallet, Bookkeeping, Token) => (req, res) => {
  if (Token) {
    Wallet.remove({ _id: req.query.wallet_id }, (error, removed) => {
      if (error) res.status(400).json(error);
      res.status(200).json({ success: true, message: 'Removed successfully' });
    })

  } else return res.status(401).send({ success: false, message: 'Unauthorized' });
}

module.exports = api;
