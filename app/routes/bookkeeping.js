const passport = require('passport');
const config = require('@config');
const models = require('@root/app/setup');

module.exports = (app) => {
  const api = app.api.bookkeeping;

  app.route('/api/v1/bookkeeping')
    .post(passport.authenticate('jwt', config.session), api.store(models.User, models.Bookkeeping, app.get('flowsecretkey')))
    .get(passport.authenticate('jwt', config.session), api.getAll(models.User, models.Bookkeeping, app.get('flowsecretkey')))
    .delete(passport.authenticate('jwt', config.session), api.remove(models.User, models.Bookkeeping, app.get('flowsecretkey')))

  app.route('/api/v1/bookkeeping/single')
    .get(passport.authenticate('jwt', config.session), api.getById(models.User, models.Bookkeeping, app.get('flowsecretkey')))
    .put(passport.authenticate('jwt', config.session), api.edit(models.User, models.Bookkeeping, app.get('flowsecretkey')))
}
