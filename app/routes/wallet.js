const passport = require('passport');
const config = require('@config');
const models = require('@root/app/setup');

module.exports = (app) => {
  const api = app.api.wallet;

  app.route('/api/v1/wallet')
    .post(passport.authenticate('jwt', config.session), api.store(models.User, models.Wallet, models.Bookkeeping, app.get('flowsecretkey')))
    .get(passport.authenticate('jwt', config.session), api.getAll(models.User, models.Wallet, app.get('flowsecretkey')))
    .get(passport.authenticate('jwt', config.session), api.getAllFromBookkeeping(models.User, models.Bookkeeping, app.get('flowsecretkey')))
    .delete(passport.authenticate('jwt', config.session), api.remove(models.User, models.Wallet, models.Bookkeeping, app.get('flowsecretkey')))

  app.route('/api/v1/wallet/single')
    .get(passport.authenticate('jwt', config.session), api.getById(models.User, models.Wallet, models.Bookkeeping, app.get('flowsecretkey')))
    .put(passport.authenticate('jwt', config.session), api.edit(models.User, models.Wallet, models.Bookkeeping, app.get('flowsecretkey')))
}
