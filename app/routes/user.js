const models = require('@root/app/setup');

module.exports = (app) => {
  const api = app.api.user;
// ! TODO delete setup path 
  app.route('/api/v1/setup')
    .post(api.setup(models.User))

  app.route('/api/v1/signup')
    .post(api.signup(models.User))
  
  app.route('/api/v1/reset')
    .post(api.sendResetMail(models.User))
    .get(api.showResetPage(models.User))
    .put(api.resetPassword(models.User))

  app.route('/expired')
  .get((req, res) => {
    res.render('expired/expired');
  })

  app.use((req,res) => {
    res.status(404).render('404/404');
  });
}