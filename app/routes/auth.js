const models = require('@root/app/setup');
module.exports = (app) => {
  const api = app.api.auth;
  app.route('/')
     .get((req, res) => res.status(400).render('api-doc/api'));
  app.route('/api/v1/auth')
     .post(api.login(models.User));
}